import * as types from '../consts'

let actions = {}

actions.showRemoveModal = () => {
    return { type: types.SHOW_REMOVE_MODAL }
}

actions.closeRemoveModal = () => {
    return { type: types.CLOSE_REMOVE_MODAL }
}

export default actions