import * as types from '../consts'

let actions = {}

actions.showWarningModal = () => {
    return { type: types.SHOW_WARNING_MODAL }
}

actions.closeWarningModal = () => {
    return { type: types.CLOSE_WARNING_MODAL }
}

actions.resetAllData = () => {
    return { type: types.RESET_ALL_DATA }
}

export default actions