import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import actions from '../actions/confirm-modal-actions'
import removeModalActions from '../actions/remove-modal-actions'
import generalInfoActions from '../../general-info/actions'

export const ConfirmModal = (props) => {
    const { actions, showConfirmModal, resultTitle, resultTitleKey, removeModalActions, generalInfoActions, machineChoice } = props
    return (
        <Modal show={showConfirmModal} style={{marginTop: '200px'}} onHide={() => actions.closeConfirmModal()}>
            <Modal.Body style={{textAlign: 'center'}}>
                <h1><i className='fas fa-exclamation-circle' style={{color:'#c9302c'}}/></h1>
                {
                    resultTitleKey === 'all'
                    ? <h3>Confirm Delete All Data?</h3>
                    : <h3>Confirm Delete {resultTitle[resultTitleKey].date} / {resultTitle[resultTitleKey].name}?</h3>
                }
            </Modal.Body>
            <Modal.Footer style={{textAlign: 'center'}}>
                {
                    resultTitleKey === 'all'
                    ? <Button 
                        bsStyle='danger'
                        style={{fontWeight: 'bold', width: '40%'}} 
                        onClick={() => {
                            actions.closeConfirmModal(); 
                            removeModalActions.closeRemoveModal();
                            generalInfoActions.deleteData(resultTitle, machineChoice)
                        }}>
                        Confirm
                    </Button>
                    : <Button 
                        bsStyle='danger'
                        style={{fontWeight: 'bold', width: '40%'}} 
                        onClick={() => {
                            actions.closeConfirmModal(); 
                            removeModalActions.closeRemoveModal();
                            generalInfoActions.deleteData(resultTitle[resultTitleKey], machineChoice)
                        }}>
                        Confirm
                    </Button>
                }
                <Button 
                    style={{fontWeight: 'bold', width: '40%'}} 
                    onClick={() => actions.closeConfirmModal()}>Cancel</Button>
            </Modal.Footer>
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {
        showConfirmModal: state.utils.showConfirmModal.show,
        resultTitle: state.generalInfo.resultTitle,
        resultTitleKey: state.utils.showConfirmModal.key,
        machineChoice: state.home.machineChoice
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch),
        removeModalActions: bindActionCreators(removeModalActions, dispatch),
        generalInfoActions: bindActionCreators(generalInfoActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmModal)