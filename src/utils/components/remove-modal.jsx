import React from 'react'
import moment from 'moment'
import { Modal, Button, DropdownButton, MenuItem } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import actions from '../actions/remove-modal-actions'
import confirmModalActions from '../actions/confirm-modal-actions'

export const RemoveModal = (props) => {
    const { actions, showRemoveModal, resultTitle, showConfirmModal, confirmModalActions } = props
    return (
        <Modal show={showRemoveModal} style={{marginTop: '200px'}} onHide={() => actions.closeRemoveModal()}>
            <Modal.Body style={{textAlign: 'center'}}>
                <h1><i className='fas fa-trash-alt' style={{color:'#c9302c'}}/></h1>
                <h3>Select Data to Delete</h3>
                <DropdownButton title='Select Data' id='drop-down' onSelect={(e) => confirmModalActions.showConfirmModal(e)}>
                    <MenuItem eventKey='all'>All</MenuItem>
                    {
                        resultTitle.map((item, key) => {
                            return (
                                <MenuItem 
                                    eventKey={key} 
                                    key={key}> {moment(item.date).format('MM/DD/YYYY')} - {item.name}
                                </MenuItem>
                            )
                        })
                    }
                </DropdownButton>
            </Modal.Body>
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {
        showRemoveModal: state.utils.showRemoveModal,
        resultTitle: state.generalInfo.resultTitle
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch),
        confirmModalActions: bindActionCreators(confirmModalActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RemoveModal)