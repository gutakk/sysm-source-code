import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { ConfigurationPage } from './components'
import actions from './actions'


const mapStateToProps = (state) => {
    return {
        machineChoice: state.home.machineChoice,
        name: state.configuration.name,
        date: state.configuration.date,
        config: state.configuration.config,
        stainingTime: state.configuration.stainingTime,
        reti: state.configuration.reti
    }
}

const mapDispatchToProps = (dispatch) => {
    return { actions: bindActionCreators(actions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfigurationPage)