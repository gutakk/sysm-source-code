import React from 'react'
import '../css/configuration.css'
import un from '../assets/images/un.png'
import xn from '../assets/images/xn.png'
import moment from 'moment'
import { FormGroup, FormControl, InputGroup, Button, Row, Col, OverlayTrigger } from 'react-bootstrap'
import { Link } from 'react-router'
import { tooltip } from '../utils/components/tooltip'

export const ConfigurationPage = (props) => {
    const { actions, machineChoice, name, date, config, stainingTime, reti } = props
    const isAllDataFill = () => {
        let isAllFill = null
        if (machineChoice === 'Urinalysis') {
            const dataValue = [
                name,
                date,
                config[0].unit,
                config[0].throughput,
                config[1].unit,
                config[1].throughput,
                config[2].unit,
                config[2].throughput
            ]
            isAllFill = dataValue.every((value) => {
                return value !== ''
            })
        }
        else {
            const dataValue = [
                name,
                date,
                config[0].unit,
                config[0].throughput,
                config[1].unit,
                config[1].throughput,
                config[2].unit,
                config[2].throughput,
                stainingTime,
                reti
            ]
            isAllFill = dataValue.every((value) => {
                return value !== ''
            })
        }
        return isAllFill
    }
    const IsLink = () => {
        if (!isAllDataFill()) {
            return (
                <OverlayTrigger placement='right' overlay={tooltip('Please fill all configuration')}>
                    <span>
                        <Button className='menu-button' disabled={!isAllDataFill()} style={{pointerEvents:'none'}}>
                            <i className='fa fa-arrow-right'/>
                        </Button>
                    </span>
                </OverlayTrigger>
            )
        }
        else {
            return (
                <OverlayTrigger placement='right' overlay={tooltip('Next to Sample Pattern')}>
                    <Link to='sample-pattern'>
                        <Button className='menu-button' disabled={!isAllDataFill()}><i className='fa fa-arrow-right'/></Button>
                    </Link>
                </OverlayTrigger>
            )
        }
    }
    return (
        <div id='configuration-page-container'>
            <div id='panel-container'>
                <Row>
                    <Col md={9}>
                        <h1>Configuration</h1>
                    </Col>
                    <Col md={3}>
                        <h5>{machineChoice === 'Urinalysis' ? 'Urinalysis Modular System' : 'Automated Hematology System'}</h5>
                        <h5>{name}</h5>
                        <h5>{date ? moment(date).format('MM / DD / YYYY') : date}</h5>
                    </Col>
                </Row>
                <hr/>
                <Row>
                    <FormGroup>
                        <Col md={5}>
                            <InputGroup>
                                <InputGroup.Addon>Name</InputGroup.Addon>
                                <FormControl 
                                    type='text'
                                    name='name'
                                    value={name}
                                    onChange={(e) => actions.changeInput(e.target.value, e.target.name)}/>
                            </InputGroup>
                        </Col>

                        <Col md={3}>
                            <InputGroup>
                                <InputGroup.Addon>Date</InputGroup.Addon>
                                <FormControl 
                                    type='date'
                                    name='date'
                                    value={date}
                                    onChange={(e) => actions.changeInput(e.target.value, e.target.name)}/>
                            </InputGroup>
                        </Col>
                    </FormGroup>
                </Row>
                {machineChoice === 'Urinalysis' && 
                    <div style={{textAlign:'center'}}>
                        <Row>
                            <img src={un} width='75%' style={{marginTop:'20px'}}/>
                        </Row>
                        <Row style={{marginTop:'10px', display:'flex', justifyContent:'center'}}>
                            <FormGroup key={0} style={{width:'15%'}}>
                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='unit'
                                        value={config[0].unit}
                                        onChange={(e) => actions.changeConfig(e.target.value, 0, e.target.name)}/>
                                    <InputGroup.Addon>Unit</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='throughput'
                                        placeholder='Throughput' 
                                        value={config[0].throughput}
                                        onChange={(e) => actions.changeConfig(e.target.value, 0, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>

                            <FormGroup key={1} style={{width:'15%', marginLeft:'4%', marginRight:'4%'}}>
                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='unit'
                                        value={config[1].unit}
                                        onChange={(e) => actions.changeConfig(e.target.value, 1, e.target.name)}/>
                                    <InputGroup.Addon>Unit</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='throughput'
                                        placeholder='Throughput' 
                                        value={config[1].throughput}
                                        onChange={(e) => actions.changeConfig(e.target.value, 1, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>

                            <FormGroup key={2} style={{width:'15%'}}>
                             <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='unit'
                                        value={config[2].unit}
                                        onChange={(e) => actions.changeConfig(e.target.value, 2, e.target.name)}/>
                                    <InputGroup.Addon>Unit</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='throughput'
                                        placeholder='Throughput' 
                                        value={config[2].throughput}
                                        onChange={(e) => actions.changeConfig(e.target.value, 2, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>
                        </Row>
                    </div>
                }

                {machineChoice === 'Automated' && 
                    <div style={{textAlign:'center'}}>
                        <Row>
                            <img src={xn} width='70%' style={{marginTop:'20px'}}/>
                        </Row>
                        <Row style={{marginTop:'10px', display:'flex', justifyContent:'center'}}>
                            <FormGroup key={0} style={{width:'15%'}}>
                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='unit'
                                        value={config[0].unit}
                                        onChange={(e) => actions.changeConfig(e.target.value, 0, e.target.name)}/>
                                    <InputGroup.Addon>Unit</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='throughput'
                                        placeholder='Throughput' 
                                        value={config[0].throughput}
                                        onChange={(e) => actions.changeConfig(e.target.value, 0, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>

                            <FormGroup key={1} style={{width:'15%', marginLeft:'4%', marginRight:'4%'}}>
                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='unit'
                                        value={config[1].unit}
                                        onChange={(e) => actions.changeConfig(e.target.value, 1, e.target.name)}/>
                                    <InputGroup.Addon>Unit</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='throughput'
                                        placeholder='Throughput' 
                                        value={config[1].throughput}
                                        onChange={(e) => actions.changeConfig(e.target.value, 1, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='stainingTime'
                                        placeholder='Staining Time' 
                                        value={stainingTime}
                                        onChange={(e) => actions.changeInput(e.target.value, e.target.name)}/>
                                </InputGroup>
                            </FormGroup>

                            <FormGroup key={2} style={{width:'15%'}}>
                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number'
                                        name='unit'
                                        value={config[2].unit}
                                        onChange={(e) => actions.changeConfig(e.target.value, 2, e.target.name)}/>
                                    <InputGroup.Addon>Unit</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='throughput'
                                        placeholder='Throughput' 
                                        value={config[2].throughput}
                                        onChange={(e) => actions.changeConfig(e.target.value, 2, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>

                                 <InputGroup style={{marginBottom:'10px'}}>
                                    <FormControl 
                                        type='number' 
                                        name='reti'
                                        placeholder='Reti' 
                                        value={reti}
                                        onChange={(e) => actions.changeInput(e.target.value, e.target.name)}/>
                                    <InputGroup.Addon>%</InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>
                        </Row>
                    </div>
                }
            </div>
            <div id='menu-container'>
                <OverlayTrigger placement='left' overlay={tooltip('Back to General Info')}>
                    <Link to='general-info'>
                        <Button className='menu-button'><i className='fa fa-arrow-left'/></Button>
                    </Link>
                </OverlayTrigger>
                <OverlayTrigger placement='top' overlay={tooltip('Reset Input')}>
                    <Button className='menu-button' style={{marginLeft:'10%', marginRight:'10%'}} onClick={() => actions.resetData()}>
                        <i className='fa fa-redo'/>
                    </Button>
                </OverlayTrigger>
                <IsLink/>
            </div>
        </div>
    )
}