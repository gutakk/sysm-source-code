import * as types from './consts'

export default function configuration(
  state = {
        name: '',
        date: '',
        config: [
            {unit: '0', throughput: ''},
            {unit: '0', throughput: ''},
            {unit: '0', throughput: ''}
        ],
        stainingTime: '',
        reti: ''
  }, action) {
    const initialState = {
        name: '',
        date: '',
        config: [
            {unit: '0', throughput: ''},
            {unit: '0', throughput: ''},
            {unit: '0', throughput: ''}
        ],
        stainingTime: '',
        reti: ''
    }
    switch (action.type) {
        case types.CHANGE_INPUT:
            return {
                ...state,
                [action.key]: action.payload
            }
        case types.CHANGE_CONFIG:
            const updatedConfig = [...state.config]
            updatedConfig[action.index][action.key] = action.payload
            return {
                ...state,
                config: updatedConfig
            }
        case types.RESET_DATA:
            return {
                ...state,
                name: initialState.name,
                date: initialState.date,
                config: initialState.config,
                stainingTime: initialState.stainingTime,
                reti: initialState.reti
            }
        default:
            return state
    }
}