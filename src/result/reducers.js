import * as types from './consts'

export default function result(
  state = {
        resultData: [],
        averageQuantity: '',
        averageMinimum: '',
        averageMaximum: '',
        averageAverage: '',
        isFromCalculate: false
  }, action) {
    switch (action.type) {
        case types.CALCULATE_RESULT:
            return {
                ...state,
                resultData: action.payload.resultData,
                averageQuantity: action.payload.avgQuantity,
                averageMinimum: action.payload.avgMinimum,
                averageMaximum: action.payload.avgMaximum,
                averageAverage: action.payload.avgAverage,
                isFromCalculate: action.payload.isFromCalculate
            }
        default:
            return state
    }
}