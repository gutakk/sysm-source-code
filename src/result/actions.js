import * as types from './consts'
import { urinalysisCalculation } from '../calculations/urinalysis'
import { automatedCalculation } from '../calculations/automated'
import { getResultData } from '../utils/database-function'

let actions = {}

actions.calculateResult = (samplePatternData, configuration, machineChoice) => {
    const { result, avgQuantity, avgMinimum, avgMaximum, avgAverage } = (machineChoice == 'Urinalysis' ? urinalysisCalculation(samplePatternData, configuration) : automatedCalculation(samplePatternData, configuration))
    return {
        type: types.CALCULATE_RESULT,
        payload: {
            resultData: result,
            avgQuantity: avgQuantity,
            avgMinimum: avgMinimum,
            avgMaximum: avgMaximum,
            avgAverage: avgAverage,
            isFromCalculate: true
        }
    }
}

actions.loadResultFromDB = (resultTitle) => async (dispatch) => {
    const response = await getResultData(resultTitle)
    dispatch ({
        type: types.CALCULATE_RESULT,
        payload: {
            resultData: response.resultData,
            avgQuantity: response.averageQuantity,
            avgMinimum: response.averageMinimum,
            avgMaximum: response.averageMaximum,
            avgAverage: response.averageAverage,
            isFromCalculate: false
        }
    })
}

export default actions