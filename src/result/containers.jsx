import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { ResultPage } from './components'
import actions from './actions'
import warningModalActions from '../utils/actions/warning-modal-actions'
import successModalActions from '../utils/actions/success-modal-actions'

const mapStateToProps = (state) => {
    return {
        resultData: state.result.resultData,
        averageQuantity: state.result.averageQuantity,
        averageMinimum: state.result.averageMinimum,
        averageMaximum: state.result.averageMaximum,
        averageAverage: state.result.averageAverage,
        isFromCalculate: state.result.isFromCalculate,
        machineChoice: state.home.machineChoice,
        result: state.result,
        configuration: state.configuration
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch),
        warningModalActions: bindActionCreators(warningModalActions, dispatch),
        successModalActions: bindActionCreators(successModalActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultPage)