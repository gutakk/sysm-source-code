import React from 'react'
import '../css/home.css'
import { Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router'

export const HomePage = (props) => {
    const { actions, machineChoice } = props
    return (
        <div id='home-page-container'>
            <Row className='content-row'>
                <Col md={12}>
                    <h1>Choose your System</h1>
                    <h2>to calculate tube / hours</h2>
                </Col>
            </Row>
            <Row className='content-row'>
                <Col md={12}>
                    <Link to='general-info'>
                        <Button
                            className='machine-button'
                            bsSize='large'
                            onClick={(e) => actions.onChooseMachine(e.target.value)}
                            value='Urinalysis'>Urinalysis Modular System</Button>
                    </Link>
                </Col>
            </Row>
            <Row className='content-row'>
                <Col md={12}>
                    <Link to='general-info'>
                        <Button 
                            className='machine-button'
                            bsSize='large'
                            onClick={(e) => actions.onChooseMachine(e.target.value)}
                            value='Automated'>Automated Hematology System</Button>
                    </Link>
                </Col>
            </Row>
        </div>
    )
}