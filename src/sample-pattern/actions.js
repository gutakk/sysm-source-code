import * as types from './consts'

let actions = {}

actions.changeQuantity = (payload, index) => {
    return {
        type: types.CHANGE_QUANTITY,
        payload: payload,
        index: index
    }
}

actions.resetData = () => {
    return {
        type: types.RESET_DATA
    }
}

export default actions