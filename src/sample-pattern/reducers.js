import * as types from './consts'

export default function samplePattern(
    state = {
        samplePatternData: [
            {time: '01.00 - 02.00', quantity: ''},
            {time: '02.00 - 03.00', quantity: ''},
            {time: '03.00 - 04.00', quantity: ''},
            {time: '04.00 - 05.00', quantity: ''},
            {time: '05.00 - 06.00', quantity: ''},
            {time: '06.00 - 07.00', quantity: ''},
            {time: '07.00 - 08.00', quantity: ''},
            {time: '08.00 - 09.00', quantity: ''},
            {time: '09.00 - 10.00', quantity: ''},
            {time: '10.00 - 11.00', quantity: ''},
            {time: '11.00 - 12.00', quantity: ''},
            {time: '12.00 - 13.00', quantity: ''},
            {time: '13.00 - 14.00', quantity: ''},
            {time: '14.00 - 15.00', quantity: ''},
            {time: '15.00 - 16.00', quantity: ''},
            {time: '16.00 - 17.00', quantity: ''},
            {time: '17.00 - 18.00', quantity: ''},
            {time: '18.00 - 19.00', quantity: ''},
            {time: '19.00 - 20.00', quantity: ''},
            {time: '20.00 - 21.00', quantity: ''},
            {time: '21.00 - 22.00', quantity: ''},
            {time: '22.00 - 23.00', quantity: ''},
            {time: '23.00 - 00.00', quantity: ''},
            {time: '00.00 - 01.00', quantity: ''}
        ]
    }, action) {
    const initialState = [
        {time: '01.00 - 02.00', quantity: ''},
        {time: '02.00 - 03.00', quantity: ''},
        {time: '03.00 - 04.00', quantity: ''},
        {time: '04.00 - 05.00', quantity: ''},
        {time: '05.00 - 06.00', quantity: ''},
        {time: '06.00 - 07.00', quantity: ''},
        {time: '07.00 - 08.00', quantity: ''},
        {time: '08.00 - 09.00', quantity: ''},
        {time: '09.00 - 10.00', quantity: ''},
        {time: '10.00 - 11.00', quantity: ''},
        {time: '11.00 - 12.00', quantity: ''},
        {time: '12.00 - 13.00', quantity: ''},
        {time: '13.00 - 14.00', quantity: ''},
        {time: '14.00 - 15.00', quantity: ''},
        {time: '15.00 - 16.00', quantity: ''},
        {time: '16.00 - 17.00', quantity: ''},
        {time: '17.00 - 18.00', quantity: ''},
        {time: '18.00 - 19.00', quantity: ''},
        {time: '19.00 - 20.00', quantity: ''},
        {time: '20.00 - 21.00', quantity: ''},
        {time: '21.00 - 22.00', quantity: ''},
        {time: '22.00 - 23.00', quantity: ''},
        {time: '23.00 - 00.00', quantity: ''},
        {time: '00.00 - 01.00', quantity: ''}
    ]
    switch (action.type) {
        case types.CHANGE_QUANTITY:
            const updatedSamplePatternData = [...state.samplePatternData]
            updatedSamplePatternData[action.index].quantity = action.payload
            return {
                ...state,
                samplePatternData: updatedSamplePatternData
            }
        case types.RESET_DATA:
            return {
                ...state,
                samplePatternData: initialState
            }
        default:
            return state
    }
}