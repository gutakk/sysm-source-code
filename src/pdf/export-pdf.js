import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'

export const exportPDF = (configuration) => {
    const elements = document.querySelectorAll('.printElement')
    const pdf = new jsPDF('landscape', 'mm', 'a4')
    const width = pdf.internal.pageSize.getWidth()   
    const height = pdf.internal.pageSize.getHeight() 
    elements.forEach((elem, idx) => {
        html2canvas(elem).then((canvas) => {
            const imgData = canvas.toDataURL('image/jpg')
            if (idx < elements.length - 1) {
                if (elem.className.includes('table')) {
                    pdf.addImage(imgData, 'JPEG', 10, 10, width-20, height-20)
                }
                else {
                    pdf.addImage(imgData, 'JPEG', 15, 20, width-20, height-50)
                }
                pdf.addPage()
            }
            else {
                if (elem.className.includes('table')) {
                    pdf.addImage(imgData, 'JPEG', 10, 10, width-20, height-20)
                }
                else {
                    pdf.addImage(imgData, 'JPEG', 15, 20, width-20, height-50)
                }
            }
            if (idx == elements.length - 1) {
                pdf.save(configuration.date + ' ' + configuration.name)  
            }
        })
    })      
}