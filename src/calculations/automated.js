import { roundUpMinute, constructResult } from './common'

export const automatedCalculation = (samplePattern, configuration) => {
    const config = configuration.config
    const firstMachineArr = []
    const secondMachineArr = []
    const thirdMachineArr = []
    const quantityArr = []

    const stainingTime = parseFloat(configuration.stainingTime)
    const reti = parseFloat(configuration.reti)
    const firstUnit = parseFloat(config[2].unit)
    const secondUnit = parseFloat(config[1].unit)
    const thirdUnit = parseFloat(config[0].unit)
    const firstThroughput = parseFloat(config[2].throughput)
    const secondThroughput = parseFloat(config[1].throughput)
    const thirdThroughput = parseFloat(config[0].throughput)

    const minimum = samplePattern.map((value) => {
        const quantity = parseFloat(value.quantity)
        return roundUpMinute(((quantity*firstThroughput/100)*60/100) + ((quantity*reti/100)*60/83))
    })

    const maximum = samplePattern.map((value) => {
        // From right to left
        const quantity = parseFloat(value.quantity)
        quantityArr.push(quantity.toFixed(0))
        
        const firstMachine = firstUnit * (((quantity*firstThroughput/100)*60/100) + ((quantity*reti/100)*60/83))
        firstMachineArr.push(firstMachine.toFixed(2))
        
        const secondMachine = secondUnit * ((60/120)*(quantity*secondThroughput/100)+stainingTime)
        secondMachineArr.push(secondMachine.toFixed(2))
        
        const thirdMachine = thirdUnit * ((60/30)*((quantity*secondThroughput/100)*thirdThroughput/100))
        thirdMachineArr.push(thirdMachine.toFixed(2))
        
        return roundUpMinute(firstMachine + secondMachine + thirdMachine)
    })

    const average = []
    for(var i=0 ; i<minimum.length ; i++) {
        const avg = ((parseFloat(minimum[i]) + parseFloat(maximum[i])) / 2).toFixed(2)
        average.push(avg)
    }
    
    return constructResult(minimum, maximum, average, quantityArr, firstMachineArr, secondMachineArr, thirdMachineArr)
}