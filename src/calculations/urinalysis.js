import { roundUpMinute, constructResult } from './common'

export const urinalysisCalculation = (samplePattern, configuration) => {
    const config = configuration.config
    const firstMachineArr = []
    const secondMachineArr = []
    const thirdMachineArr = []
    const quantityArr = []

    const minimum = samplePattern.map((value) => {
        return roundUpMinute((60/276) * (parseFloat(value.quantity)*parseFloat(config[2].throughput)/100))
    })

    const maximum = samplePattern.map((value) => {
        // From right to left
        const quantity = parseFloat(value.quantity)
        quantityArr.push(quantity.toFixed(0))

        const firstMachine = parseFloat(config[2].unit) * ((60/276)*(parseFloat(quantity)*parseFloat(config[2].throughput)/100))
        firstMachineArr.push(firstMachine.toFixed(2))

        const secondMachine = parseFloat(config[1].unit) * ((60/105+2)*(parseFloat(quantity)*parseFloat(config[1].throughput)/100))
        secondMachineArr.push(secondMachine.toFixed(2))
        
        const thirdMachine = parseFloat(config[0].unit) * ((60/50)*((parseFloat(quantity)*parseFloat(config[1].throughput)/100)*parseFloat(config[0].throughput)/100))
        thirdMachineArr.push(thirdMachine.toFixed(2))
        
        return roundUpMinute(firstMachine + secondMachine + thirdMachine)
    })

    const average = []
    for(var i=0 ; i<minimum.length ; i++) {
        const avg = ((parseFloat(minimum[i]) + parseFloat(maximum[i])) / 2).toFixed(2)
        average.push(avg)
    }
    
    return constructResult(minimum, maximum, average, quantityArr, firstMachineArr, secondMachineArr, thirdMachineArr)
}