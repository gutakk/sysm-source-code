export const roundUpMinute = (data) => {
    var twoDecimal = data.toFixed(2)
    var decimal = (twoDecimal - Math.floor(data)).toFixed(2)
    if (parseFloat(decimal) >= 0.60) {
        twoDecimal = parseFloat(twoDecimal) + 1 - parseFloat(decimal)
        decimal = parseFloat(decimal) - 0.60
    }
    const result = (Math.floor(parseFloat(twoDecimal)) + parseFloat(decimal)).toFixed(2)
    return result
}

export const constructResult = (minimum, maximum, average, quantity, firstMachine, secondMachine, thirdMachine) => {
    const time = [
        '01.00 - 02.00',
        '02.00 - 03.00',
        '03.00 - 04.00',
        '04.00 - 05.00',
        '05.00 - 06.00',
        '06.00 - 07.00',
        '07.00 - 08.00',
        '08.00 - 09.00',
        '09.00 - 10.00',
        '10.00 - 11.00',
        '11.00 - 12.00',
        '12.00 - 13.00',
        '13.00 - 14.00',
        '14.00 - 15.00',
        '15.00 - 16.00',
        '16.00 - 17.00',
        '17.00 - 18.00',
        '18.00 - 19.00',
        '19.00 - 20.00',
        '20.00 - 21.00',
        '21.00 - 22.00',
        '22.00 - 23.00',
        '23.00 - 00.00',
        '00.00 - 01.00'
    ]
    const result = []
    let avgQuantity = 0
    let avgMinimum = 0
    let avgMaximum = 0
    let avgAverage = 0
    for(var i=0 ; i<time.length ; i++) {
        const item = {
            time: time[i],
            quantity: quantity[i],
            minimum: minimum[i],
            maximum: maximum[i],
            average: average[i],
            firstMachine: firstMachine[i],
            secondMachine: secondMachine[i],
            thirdMachine: thirdMachine[i]
        }
        result.push(item)

        avgQuantity += parseFloat(quantity[i])
        avgMinimum += parseFloat(minimum[i])
        avgMaximum += parseFloat(maximum[i])
        avgAverage += parseFloat(average[i])
    }

    avgQuantity = (avgQuantity/time.length).toFixed(2)
    avgMinimum = (avgMinimum/time.length).toFixed(2)
    avgMaximum = (avgMaximum/time.length).toFixed(2)
    avgAverage = (avgAverage/time.length).toFixed(2)
    
    return { result, avgQuantity, avgMinimum, avgMaximum, avgAverage }
}

export const constructGraphResult = (resultData, graphType, machine) => {
    const graphResult = []
    if(graphType == 'each') {
        for(var i=0 ; i<resultData.length ; i++) {
            const item = {
                name: resultData[i].time,
                time: parseFloat(resultData[i][machine])
            }
            graphResult.push(item)
        }
    }
    else {
        for(var i=0 ; i<resultData.length ; i++) {
            const item = {
                name: resultData[i].time,
                minimum: parseFloat(resultData[i].minimum),
                maximum: parseFloat(resultData[i].maximum),
                average: parseFloat(resultData[i].average)
            }
            graphResult.push(item)
        }
    }
    
    return graphResult
}