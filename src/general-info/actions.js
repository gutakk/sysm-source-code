import * as types from './consts'
import { getAllResultTitle, deleteSingleData, deleteAllData } from '../utils/database-function'
import _ from 'lodash'

let actions = {}

actions.getAllResultTitle = (machineChoice) => async (dispatch) => {
    const allResultTitle = await getAllResultTitle(machineChoice)
    const sortedAllResultTitle = _.sortBy(allResultTitle, ['date'])
    dispatch ({
        type: types.GET_ALL_RESULT_TITLE,
        payload: sortedAllResultTitle
    })
}

actions.changeSearch = (payload, key) => {
    return {
        type: types.CHANGE_SEARCH,
        payload: payload,
        key: key
    }
}

actions.deleteData = (resultTitle, machineChoice) => async (dispatch) => {
    if (_.isArray(resultTitle)) {
        const response = await deleteAllData(resultTitle)
    }
    else {
        const response = await deleteSingleData(resultTitle)
    }
    const allResultTitle = await getAllResultTitle(machineChoice)
    const sortedAllResultTitle = _.sortBy(allResultTitle, ['date'])
    dispatch ({
        type: types.GET_ALL_RESULT_TITLE,
        payload: sortedAllResultTitle
    })
}

export default actions