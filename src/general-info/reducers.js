import * as types from './consts'

export default function generalInfo(
  state = {
      resultTitle: [],
      loading: true,
      searchName: '',
      searchDate: ''
  }, action) {
    switch (action.type) {
        case types.GET_ALL_RESULT_TITLE:
            return {
                ...state,
                resultTitle: action.payload,
                loading: false
            }
        case types.CHANGE_SEARCH:
            return {
                ...state,
                [action.key]: action.payload
            }
        default:
            return state
    }
}